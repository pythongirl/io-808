import React from "react";
import PropTypes from 'prop-types';
import Radium from "radium";
import Octicon from "react-octicon";
import { saveAs } from "file-saver";

import Button from "components/button";


import { PERSISTANCE_FILTER } from "store-constants";
import { buttonColor, darkGrey } from "theme/variables";

import { labelGreyLarge } from "theme/mixins";


class SaveButton extends React.Component {
  static propTypes = {
    storeState: PropTypes.object.isRequired,
    size: PropTypes.number
  };

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const storeState = this.props.storeState;

    // only save properties defined by persistance filter
    const saveObj = {};
    PERSISTANCE_FILTER.forEach(key => {
      saveObj[key] = storeState[key];
    });

    const saveString = JSON.stringify(saveObj);
    const saveData = new Blob([saveString], {
      type: "text/plain;charset=utf-8"
    });
    saveAs(saveData, "io808.json");
  }

  render() {
    const { storeState, size = 50 } = this.props;

    const styles = {
      button: {
        width: "auto",
        height: size,
        borderRadius: 4,
        backgroundColor: buttonColor,
        marginLeft: 5,
        marginRight: 5,
        padding: 7,
        ...labelGreyLarge,
        color: darkGrey,
        display: "flex",
        alignItems: "center",
      }
    };

    return (
      <Button
        style={styles.button}
        disabled={storeState.playing}
        onClick={this.handleClick}
      >
        Save
      </Button>
    );
  }
}

export default Radium(SaveButton);
